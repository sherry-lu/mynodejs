// require()是commonJS中的方法，专门加载JS模块的，先安装：在终端运行npm install express --save
const express = require("express");

// 日志模块，先安装：在终端运行npm install morgan --save
const logger = require("morgan");
const session=require("express-session")

// 自定义的路由模块
const userRouter = require("./routes/userRouter");

// 创建一个应用程序服务
const app = express();

// 使用logger日志的格式为开发模式 --dev开发模式
// 写在最前面，进入服务器就记载登录日志
app.use(logger("dev"));

// 配置静态资源目录 __dirname表示当前项目的根目录（这里是mynodejs的绝对路径）
// 也表示相对路径（字符串拼接），__dirname就进入了mynodejs这个文件夹 /public 进入文件夹public
// 一般之前写的项目文件都放在这里面，public目录下，要直接放一个首页的文件（名字一般为：index.html  default.html）
// ,{index:"文件名不受限制"}
// app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/public",{index:"index2.html"}));

// get 请求组模块
app.get("/login", function (request, response) {
  console.log("收到请求");
  response.write("hello");
  response.end();
});
// post请求模块，不能获取参数，就需要配置下列两行
app.use(express.json({ limit: "2024kb" })); //定义数据格式为json格式，解析传入的请求是JSON格式，limit，控制body参数的大小，最大体积，若超过会报错
app.use(express.urlencoded({ extended: true })); //读取post数据，默认参数是false true代表更高级别的文件处理方式
// 解析参数使用不同的库:
// false:querystring 简单的json对象；对象中属性值同类型
// true:qs 解析复杂的内容；对象中属性为不同类型

// 表示处理 以“/”开头的接口请求，都可以处理
// app.use（基础路径，模块名（相应的js文件名））对应const userRouter
app.use("/", userRouter);

// 不能使用1024之前的端口号（windows系统预留端口）
// 不能使用软件已占用的端口号，3000（qq）
// // 访问服务器的方法：
// http://172.16.141.175:8888/  http://局域网IP地址：端口号  其他同学可以通过局域网来访问你。
// http://127.0.0.1:8888        http://本机默认IP：端口号  只能自己访问自己电脑上的服务器
// http://localhost:8888        http://localhost：端口号  只能自己访问自己电脑上的服务器
app.listen(8888, () => {
  console.log("服务器运行中，点击http://127.0.0.1:8888 访问");
});

// 安装mysql模块, npm i mysql --save
