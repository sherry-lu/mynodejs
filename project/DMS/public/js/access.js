window.onload = getAccess;
function getAccess() {
  let xhr = null;
  if (window.XMLHttpRequest) {
    xhr = new XMLHttpRequest(); // 非IE浏览器，现代浏览器的写法
  } else {
    xhr = new ActiveXObject("Microsoft.XMLHTTP"); // IE11/10/9
  }
  xhr.open("get", "/access/getAccess", true);
  xhr.send();
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      let res = JSON.parse(xhr.responseText);
      let arr = res.data;
      if (res.code === 0) {
        let htmlStr = "";
        let accessList = document.querySelector("#accessList");
        arr.forEach((element) => {
          htmlStr += `<tr>
                          <td>
                              <input type="checkbox" />
                          </td>
                          <td>${element.pname}</td>
                          <td>${element.pdes}</td>
                          <td>
                              <button>批量授权</button>
                              <button data-id=${element.pid} id="delBtn">删除</button>
                          </td>
                      </tr>`;
        });
        console.log(`accessList`, accessList)
        accessList.innerHTML = htmlStr;
      } else {
        alert(res.msg);
      }
    }
  };
}
// #NOTE:添加功能
$("#addBtn").on("click", function () {
  let pname = $("[name='pname']").val();
  let pdes = $("[name='pdes']").val();
  if (pname && pdes) {
    $.ajax({
      type: "post",
      url: "/access/add",
      data: { pname, pdes },
      success(res) {
        if (res.code === 0) {
          alert(res.msg);
          // 重新渲染页面
          getAccess();
          $("[name='addForm']").get(0).reset();
        } else {
          alert(res.msg);
        }
      },
    });
  } else {
    alert("必填");
  }
});
getAccess();
// #NOTE:删除功能
$("#accessList").on("click", "#delBtn", delHandler);
// 函数：删除
function delHandler() {
  // 1. 确认复选框 已勾选
  let checkboxInp = $(this).parents("tr").find(":checkbox");
  // console.log(`checkboxInp`, checkboxInp)
  let isChecked = checkboxInp.get(0).checked;
  if (!isChecked) {
    alert("删除前，请勾上要删除的对象");
    return;
  }
  // 2. 获取data-id
  let pid = $(this).attr("data-id");
  console.log(`pid`, pid);
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    // 确认ajax流程走完，并获取到了相应的资源
    if (xhr.readyState === 4 && xhr.status === 200) {
      let res = JSON.parse(xhr.responseText);
      if (res.code === 0) {
        // 渲染网页
        getAccess();
        alert(res.msg);
      } else {
        // 4001 可能原因：没有找到、无法删除
        alert(res.msg);
      }
    }
  };
  xhr.open("post", "/access/delAccess", true);
  xhr.setRequestHeader(
    "Content-type",
    "application/x-www-form-urlencoded"
  );
  xhr.send("acid=" + pid);
}
// #NOTE:搜索功能
let searchInp = document.getElementById("searchInp");
let searchBtn = document.getElementById("searchBtn");
searchBtn.onclick = function () {
  let searchValue = searchInp.value.trim();
  // 判空
  if (!searchValue) {
    return;
  }
  let xhr = new XMLHttpRequest();
  // #TODO:为什么这么写？
  // 搜索接口
  xhr.open("get", "/access/find?text=" + searchValue, true);
  xhr.send();
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      let res = JSON.parse(xhr.responseText);
      if (res.code === 0) {
        alert(res.msg);
        console.log(`res`, res);
      } else {
        alert(res.msg);
      }
    }
  };
};
