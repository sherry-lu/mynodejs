let loginBtn = document.querySelector("#loginBtn");
let usernameInp = document.querySelector("#username");
let passwordInp = document.querySelector("#password");

// #NOTE:点击提交
loginBtn.onclick = function () {
  // #NOTE:前端
  // #TODO:优化验证逻辑
  // 放到里面来获取，才能每次获取新的
  //   let username = usernameInp.value;
  //   let password = passwordInp.value;
  verifyInfo(usernameInp);
  verifyInfo(passwordInp);
  // #NOTE:to 后端
};

/* 
let btnLogin = document.getElementById("btnLogin");
      btnLogin.onclick = function () {
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;
        let xhr = new XMLHttpRequest();
        xhr.open("post", "/login", true);
        xhr.setRequestHeader(
          "Content-type",
          "application/x-www-form-urlencoded"
        );
        xhr.send(`username=${username}&password=${password}`);
        xhr.onreadystatechange = function () {
          if (xhr.readyState === 4 && xhr.status === 200) {
            let res = JSON.parse(xhr.responseText);
            if (res.code === 0) {
              console.log(res);
              sessionStorage.setItem(
                "userinfo",
                JSON.stringify(res.dataToFront[0])
              );
              location.href = "/html/main.html";
            } else {
              console.log(res);
            }
          }
        };
      };
*/

// #NOTE:function，验证username password
function verifyInfo(inputDom) {
  let value = inputDom.value;
  if (!value) {
    inputDom.classList.remove("is-valid");
    inputDom.classList.add("is-invalid");
  } else {
    inputDom.classList.remove("is-invalid");
    inputDom.classList.add("is-valid");
  }
}

// 1. 绑定事件
loginBtn.onclick = function () {
  let username = document.getElementById("username").value;
  let password = document.getElementById("password").value;
  // console.log( username, password );
  // 2. 创建XHR对象
  let xhr = new XMLHttpRequest(); // 创建AJAX引擎的实例化对象
  // 3. 链接后台服务器
  xhr.open("post", "/login", true); // 如果url没有IP:port，那么表示请求的是自己的服务器

  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); // POST需要重新设置请求头
  xhr.send(`username=${username}&password=${password}`);

  // 4. 接收后台数据
  xhr.onreadystatechange = function () {
    // console.log( xhr );
    if (xhr.readyState === 4 && xhr.status === 200) {
      let res = JSON.parse(xhr.responseText); // 获取返回的数据， 数据格式默认时文本格式。

      if (res.code === 0) {
        //登录成功
        console.log(res);
        sessionStorage.setItem("userinfo", JSON.stringify(res.dataToFront[0]));
        // localStorage ，永久存储，除非手动清除或者卸载浏览器时，提示你清除。
        // sessionStorage ，临时存储，关闭浏览器，就自动删除。
        location.href = "/html/access.html";
      } else {
        //登录失败
        console.log(res);
      }
    }
  };
};
