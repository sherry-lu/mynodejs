// NOTE: 动态渲染table内容
// NOTE: 1. 后端分页，查询用户列表数据：
let page = 1; // 当前第一页
let limit = 2; // 每页2条数据
let count = 0; //总页数，默认0
let usersArr = []; // 存储当前分页的数据
let imgFile = null; // 文件上传用的变量：
let nowCount = 0;
// 函数： 从后端获取数据（原生js ajax写法）
const getUserList = () => {
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      let res = JSON.parse(xhr.responseText);
      if (res.code === 0) {
        console.log(res); // {code, msg, count, data}
        count = Math.ceil(res.count / limit);
        usersArr = res.data; // 赋值给变量，目的，再查看，修改时，可以复用。
        nowCount = res.data.length;
        renderTable(res.data); // 渲染表格
        renderPage(count); //渲染分页
      } else {
        console.log(res);
      }
    }
  };
  xhr.open("get", "/user/getAll?pageNow=" + page + "&pageSize=" + limit);
  xhr.send();
};
window.onload = getUserList;
// // 给分页页码，做事件委托， 不要在动态渲染的函数中，做事件委托，避免多次事件的绑定。
// $(document).on("click", ".page-link", function () {
//   let event = arguments[0] || window.event;
//   event.preventDefault(); // 阻止默认行为。
//   console.log(this);
//   pageNow = Number($(this).text());
//   getUserList(); // 这个函数中，就没有参数，拿的是全局变量。
// });
// NOTE: 函数 渲染表格
function renderTable(arr) {
  let userTbody = document.getElementById("userTbody");
  userTbody.innerHTML = ""; // 每次渲染前，先清空
  let strHtml = ""; // 需要渲染的内容
  arr.forEach((el, index) => {
    strHtml += `<tr data-id="${el.uid}">
                  <td>
                    <input type="checkbox">
                  </td>
                  <td>${el.uaccess}</td>
                  <td>${el.uname}</td>
                  <td>
                    <div class="custom-control custom-switch">
                      <input type="checkbox" class="ustate-input custom-control-input" id="customSwitch${index}" ${
      el.ustate === "1" ? "checked" : ""
    }>
                      <label class="custom-control-label" for="customSwitch${index}">${
      el.ustate === "1" ? "启用" : "禁用"
    }</label>
                    </div>
                  </td>
                  <td>
                    <button type="button" class="btn btn-link text-info btn-roles">角色选择</button>
                    <button type="button" class="btn btn-link text-danger btn-delete">删除</button>
                    <button type="button" class="btn btn-link text-primary btn-modify">修改</button>
                  </td>
                </tr>`;
  });
  userTbody.innerHTML = strHtml;
}
// NOTE: 函数 渲染分页
function renderPage(count) {
  let strHtml = "";
  let userPagination = document.getElementById("userPagination");
  userPagination.innerHTML = "";
  strHtml += `<li class="page-item  ${page === 1 ? "disabled" : ""}">
                <a class="page-link" href="#" aria-label="Previous">
                  <span aria-hidden="true">Previous</span>
                </a>
              </li>`;
  for (let i = 1; i <= count; i++) {
    strHtml += ` <li class="page-item ${
      i === page ? "active" : ""
    }"><a class="page-link" href="#">${i}</a></li>`;
  }
  strHtml += `<li class="page-item ${page === count ? "disabled" : ""}">
                <a class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true">Next</span>
                </a>
              </li>`;
  userPagination.innerHTML = strHtml;
}

// NOTE: 修改
// 事件的委托：
$(document).on("click", ".btn-modify", showModifyModal);
// $( document ).on( 'click', ".btn-roles", showSelectModal)
// $( document ).on( 'click', ".btn-delete", showDeleteModal)

// 再修改用户的Modal里面，文件上传的事件绑定。
// $( "#customFile2" ).on( 'change', function () { } )
$("#customFile2").change(showFile);
let userId = null;
// 显示修改的modal:
function showModifyModal() {
  $("#modalModifyUser").modal("show"); // 1. 显示modal
  let uid = $(this).parents("tr").attr("data-id"); // 2. 获取ID
  userId = Number(uid);
  // console.log( uid ) //"1"
  let userInfo = {};

  usersArr.forEach((el, index, arr) => {
    // 3. 循环查找和uid相同的值的对象。
    // console.log( el )
    if (el.uid === Number(uid)) {
      userInfo = el;
    }
  });
  // console.log( userInfo )

  // 4. 把用户信息，显示到表单中；
  $("#uacessno2").val(userInfo.uaccess);
  $("#nickname2").val(userInfo.uname);
  $("#password2").val(userInfo.upassword);
  // document.表单名称 => 表单对象
  // 表单对象.表单域成员的名称 =》表单域成员对象
  document.modifyUserInfoForm.usex.value = userInfo.usex;
  document.modifyUserInfoForm.utel.value = userInfo.utel;
  document.modifyUserInfoForm.usid.value = userInfo.usid;
  console.log(`userInfo.uphoto`, userInfo.uphoto);
  $("#userModifyPhoto").attr("src", "/uploads/" + userInfo.uphoto);
}

// 预览图片文件
function showFile() {
  // console.log( this.value ) // C:\fakepath\dayuhaitang.webp,我们不能直接你硬盘里面的文件在浏览器里面显示的。

  if (this.files.length === 0) {
    alert("请选择一个图片");
    imgFile = "";
    return;
  } else {
    imgFile = this.files[0];
    console.log(this.files); // FileList类型的数组，里面每一个元素，都是上传的文件对象。
  }

  // 判断上传的文件类型：
  let tempArr = imgFile.name.split("."); // ["mn","sdfs","sdds","webp"]
  let fileExtendName = tempArr[tempArr.length - 1];
  if (
    fileExtendName !== "png" &&
    fileExtendName !== "gif" &&
    fileExtendName !== "jpg" &&
    fileExtendName !== "jpeg" &&
    fileExtendName !== "webp"
  ) {
    alert("请选择图片，在上传。文件类型支持：.png,.jpg,.jpeg,.gif, .webp ");
    imgFile = "";
    return;
  }

  // NOTE: 预览图片
  // https://developer.mozilla.org/zh-CN/docs/Web/API/FileReader
  let reader = new FileReader(); //FileReader 对象允许 Web 应用程序异步读取存储在用户计算机上的文件（或原始数据缓冲区）的内容，
  reader.readAsDataURL(imgFile); // 开始读取指定的Blob中的内容。一旦完成，result属性中将包含一个data: URL 格式的 Base64 字符串以表示所读取文件的内容。
  // 当转base64编码完成时，就执行下面的事件
  reader.onload = function (e) {
    // 由于读取文件时异步的，所以必须通过Onload事件来那最后的结果。
    console.log(e);
    var base64str = e.target.result; // “data:image/png;base64,UklGRk...."
    // 直接把base64字符串，赋值img.src，就可以预览图片了。
    document.getElementById("userModifyPhoto").src = base64str;
  };
}
// NOTE:确认修改
// BUG: 清空模态框
$("#modifyConfirmBtn").click(modifyUserInfoFn);
// 提交修改的数据
function modifyUserInfoFn() {
  if (!imgFile) {
    alert("请选择一个头像上传。");
    return;
  }
  // 获取表单数据
  let userInfo = {};
  userInfo.uaccess = $("#uaccessno2").val();
  userInfo.uname = $("#nickname2").val();
  userInfo.upassword = $("#password2").val();
  userInfo.usex = $("input[name='usex']:checked").val();
  userInfo.usid = $("#sid2").val();
  userInfo.utel = $("#tel2").val();
  userInfo.uphoto = imgFile;
  userInfo.uid = userId;
  userInfo.ustate = "1";
  let myFormData = new FormData();
  for (let key in userInfo) {
    myFormData.append(key, userInfo[key]);
  }
  // ajax请求
  $.ajax({
    type: "post",
    url: "/user/modifyUserInfo",
    data: myFormData,
    contentType: false,
    processData: false,
    success(res) {
      if (res.code === 0) {
        alert(res.msg);
        $("#modalModifyUser").modal("hide");
        getUserList();
      } else {
        alert(res.msg);
      }
    },
  });
}

// NOTE: 用户状态改变
$(document).on("click", ".ustate-input", function () {
  let ustate = this.checked ? "1" : "2";
  // console.log(`ustate`, ustate)
  let uid = Number($(this).parents("tr").attr("data-id"));
  // console.log(`uid`, uid)
  $.ajax({
    type: "post",
    url: "/user/changeState",
    data: { ustate, uid },
    success(res) {
      if (res.code === 0) {
        alert(res.msg);
        getUserList();
      } else {
        alert(res.msg);
      }
    },
  });
});
// NOTE: 切换页码
// FIXME: 如果是左右符号怎么办？
// FIXME: 切换页码，内容高度不变
$(document).on("click", ".page-link", function (event) {
  event.preventDefault();
  let text = this.innerText;
  if (text !== "Previous" && text !== "Next") {
    if (Number(text) === page) {
      return;
    } else {
      page = Number(text);
    }
  } else if (text === "Next" && page < count) {
    page++;
  } else if (text === "Previous" && page > 1) {
    page--;
  } else {
    return;
  }
  getUserList();
});
// #NOTE: 删除用户
$(document).on("click", ".btn-delete", function () {
  let uid = Number($(this).parents("tr").attr("data-id"));
  $.ajax({
    type: "post",
    url: "/user/delete",
    data: { uid },
    success(res) {
      if (res.code === 0) {
        alert(res.msg);
        nowCount--; // 当前页减少一天数据
        nowCount === 0 ? page-- : "";
        page < 1 && (page = 1);
        getUserList();
      } else {
        alert(res.msg);
      }
    },
  });
});
// NOTE: 添加用户
$(".add-btn").on("click", showAddModal);
function showAddModal() {
  $("#modalAddUser").modal("show");
}
$("#addConfirmBtn").on("click", function () {
  // 获取表单数据
  let userInfo = {};
  userInfo.uid = null;
  userInfo.uaccess = $("#uaccessno2").val();
  userInfo.uname = $("#nickname2").val();
  userInfo.upassword = $("#password2").val();
  userInfo.usex = $("input[name='usex']:checked").val();
  userInfo.usid = $("#sid2").val();
  userInfo.utel = $("#tel2").val();
  userInfo.uphoto = imgFile;
  userInfo.uid = userId;
  userInfo.ustate = "1";
  let myFormData = new FormData();
  for (let key in userInfo) {
    myFormData.append(key, userInfo[key]);
  }
  $.ajax({
    type: "post",
    url: "/user/add",
    data: myFormData,
    contentType: false,
    processData: false,
    success(res) {
      if (res.code === 0) {
        alert(res.msg);
        $("#addModifyUser").modal("hide");
        getUserList();
      } else {
        alert(res.msg);
      }
    },
  });
});
// NOTE: 角色选择模态框
$(document).on("click", ".btn-roles", async function () {
  let uid = Number($(this).parents("tr").attr("data-id"));
  let uaccess = "";
  usersArr.forEach((el) => {
    if (el.uid === uid) {
      uaccess = el.uaccess;
    }
  });

  $("#userName1").val(uaccess);
  // 1. 调用用户角色接口
  userRoles = await getUserForRoles(uid);
  roles = await getRoles();
  console.log(userRoles, roles);
  // 2. 调用所有用户角色接口

  // 1. 渲染角色选择
  // 2. 确认 checkbox 状态
  let rolesTable = $("#rolesTable");
  let htmlStr = "";
  let urnameArrChecked = [];
  // console.log(`userRoles.length,roles`, userRoles.length, roles.length);
  let isAll = userRoles.length === roles.length ? true : false;
  console.log(`isAll`, typeof isAll);
  $("#checkAllAccess").prop("checked", isAll);

  rolesTable.html(""); // 渲染前先清空表格内容

  roles.forEach((el) => {
    htmlStr += ` <tr>
                <td><input type="checkbox" value="${el.rid}" ${
      checkRid(el.rid, userRoles) ? "checked" : ""
    } ></td>
                <td>${el.rname}</td>
                <td>${el.rdes}</td>
              </tr>`; // 确认 checked 状态，另外写一个函数
    userRoles.forEach((item) => {
      if (item.rid === el.rid) {
        urnameArrChecked.push(el.rname);
      }
    });
  });
  $("#selectRoles2").empty();
  $("#selectRoles2").val(urnameArrChecked.toString()); //渲染 选择角色input框的内容
  rolesTable.html(htmlStr);
  $("#modalSelectRoles").modal("show");

  // 确认事件
  $("#rolesConfirmBtn").on("click", function () {
    // 给用户设置角色
    /**
     * @api: "/user/addRoles"
     * @method 'post'
     * @params uid, {Number},required
     * @params ridList, {Array},required,不能为空数组 [1,3,6,9]
     */
    console.log(`userRoles`, userRoles);
    let ridList = [];
    userRoles.forEach((el) => {
      ridList.push(el.rid);
    });
    console.log(`ridList`, ridList);
    $.ajax({
      url: "/user/addRoles",
      type: "post",
      data: { uid, ridList },
      success(res) {
        if (res.code === 0) {
          alert(res.msg);
          $("#modalSelectRoles").modal("hide");
        } else {
          alert(res.msg);
        }
      },
    });
  });
});

async function getRoles() {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "/roles/getAll",
      type: "get",
      data: {},
      success(res) {
        if (res.code === 0) {
          resolve(res.data);
        } else {
          reject(res.msg);
        }
      },
    });
  });
}
async function getUserForRoles(uid) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "/user/getRoles",
      type: "post",
      data: { uid },
      success(res) {
        if (res.code === 0) {
          resolve(res.data);
        } else {
          reject(res.msg);
        }
      },
    });
  });
}
// NOTE: 确认角色的选中状态
function checkRid(rid, userRoles) {
  let flag = false;
  userRoles.forEach((el) => {
    if (rid === el.rid) flag = true;
  });
  return flag;
}
// NOTE: 选择角色 渲染
$("#checkAllAccess").on("click", function () {
  let inputArr = document.querySelectorAll("#rolesTable input");
  if ($(this).prop("checked")) {
    // 全选
    userRoles = [];
    roles.forEach((el) => {
      userRoles.push({ rid: el.rid });
      // console.log(`userRoles`, userRoles);
    });
    for (let i = 0; i < inputArr.length; ++i) {
      inputArr[i].checked = true;
    }
    renderRoleInput();
  } else {
    // 取消全选
    userRoles = [];
    for (let i = 0; i < inputArr.length; ++i) {
      inputArr[i].checked = false;
    }
    renderRoleInput();
  }
});
$(document).on("click", "#rolesTable input", function () {
  let operateRole = Number($(this).val());
  // console.log(`operateRole`, operateRole);
  let isChecked = $(this).prop("checked");
  // console.log(`isChecked`, isChecked);
  if (isChecked) {
    // 添加
    userRoles.push({ rid: operateRole });
  } else {
    // 删除

    userRoles.forEach((el, i) => {
      if (el.rid === operateRole) {
        userRoles.splice(i, 1);
      }
    });
  }
  // console.log(`操作后的`, userRoles);

  if (userRoles.length === roles.length) {
    $("#checkAllAccess").prop("checked", true);
  } else {
    $("#checkAllAccess").prop("checked", false);
  }
  renderRoleInput();
});
$("#rolesConfirmBtn").on("click", function () {});
//
function renderRoleInput() {
  let urnameArrChecked = [];
  roles.forEach((el) => {
    userRoles.forEach((item) => {
      if (item.rid === el.rid) {
        urnameArrChecked.push(el.rname);
      }
    });
  });
  $("#selectRoles2").val(urnameArrChecked.toString());
}
