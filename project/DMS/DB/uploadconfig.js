const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./public/uploads");
  },
  filename: function (req, file, cb) {
    let fileFormat = file.originalname.split(".");
    console.log(
        fileFormat[0].slice(0, 5) +
          "_" +
          Date.now() +
          "." +
          fileFormat[fileFormat.length - 1]
      );
    cb(
      null,
      fileFormat[0].slice(0, 5) +
        "_" +
        Date.now() +
        "." +
        fileFormat[fileFormat.length - 1]
    );

  },
});
const upload = multer({
  storage: storage,
});
module.exports = upload;
