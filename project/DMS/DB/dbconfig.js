const mysql = require("mysql");
module.exports = {
  config: {
    host: "localhost", //目标数据库所在电脑的IP地址
    // host:"localhost",
    port: "3306",
    user: "root",
    password: "123qwe",
    database: "mydoctor", //数据库的名字
    multipleStatements: true, //支持执行多条sql语句
    timezone: "08:00", //设置时区，如果不匹配，就可能有时差
  },
  connect(sql, params, cb) {
    let conn = mysql.createConnection(this.config); //创建一个mysql的连接
    conn.connect(); //打开连接
    conn.query(sql, params, cb); //执行mysql语句
    conn.end(); //关闭连接
  },
};