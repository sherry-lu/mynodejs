const fs = require("fs");
// 参数1：
// 参数2：读取文件时采用的编码格式，默认是Buffer，一般采用utf-8
// 回调函数，拿到读取失败和成功的结果 err dataStr（指定传回2个参数，可自命名，类似event）
fs.readFile("../resource/docs/1.txt", "utf-8", function (err, dataStr) {
  console.log(`err`, err); //null
  console.log(`dataStr`, dataStr); //12344
});
