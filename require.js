const m1 = require("./index.js"); // -> 请求访问，加载用户自定义的模块，会执行模块内的代码，可以省略.js后缀名
console.log(`m1`, m1); // m1 本身是个空对象，模块作用域的问题，模块内的变量无法再模块外访问到
console.log(`module`, module);// module对象，默认exports:{}，可通过module.exports将模块内的成员共享出去
